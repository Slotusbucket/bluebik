package com.bluebik.car.musclecar;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import com.bluebik.car.util.CustomHibernateDaoSupport;

/**
 * Copyright © 2016 Bluebik Group.
 * Created by khakhanat on 24/10/2017 AD.
 */
public class MuscleCarDaoImpl extends CustomHibernateDaoSupport implements MuscleCarDao {

    public MuscleCarDaoImpl() {
    }

    @Override
    public MuscleCar getCarFromList(int id) {
        List list = getHibernateTemplate().find(
                     "from MuscleCar where id=?",id
        );
		return (MuscleCar)list.get(0);
    }

    @Override
    public void removeCarFromList(int id) {
        MuscleCar muscleCar = new MuscleCar();
        muscleCar.setId(id);
		getHibernateTemplate().delete(muscleCar);
    }

    @Override
    public void addCarToList(MuscleCar muscleCar) {
    	getHibernateTemplate().save(muscleCar);
    }

    @Override
    public void updateCarFromList(int id, MuscleCar muscleCar) {
		getHibernateTemplate().update(muscleCar);
    }

    @Override
    public List<MuscleCar> listAllCars() {
    		        List list = getHibernateTemplate().find(
                     "from MuscleCar"
        );
		return list;
    }

}
